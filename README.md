![Laravel Logo](img/laravel.png "Laravel Logo")

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Laravel 8 Image Upload with Preview
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-8-image-upload-with-preview.git`
2. Go inside the folder: `cd laravel-8-image-upload-with-preview`
3. Run `cp .env.example .env` then put your database name, credentials & AWS Key.
4. Run `composer install`
5. Run `php artisan key:generate`
6. Run `php artisan migrate`
7. Run `php artisan serve`
8. Open your favorite browser: http://localhost:8000

### Screen shot

Laravel 8 Home Page

![Laravel 8 Home Page](img/home.png "Laravel 8 Home Page")

Upload Image

![Upload Image](img/upload1.png "Upload Image")

![Upload Image](img/upload2.png "Upload Image")

![Upload Image](img/upload3.png "Upload Image")
