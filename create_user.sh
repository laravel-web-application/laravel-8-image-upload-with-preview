#!/bin/bash
# create a new user and add it to sudoers list

if [ $(id -u) -eq 0 ]; then
    if [ -z "$1" ]; then
        # Exit if no arguments!
        echo "No arguments supplied. At least an argument is required!"
        exit 2
    else
        username="$1"
        if [ -z "$2" ]; then
            password="$1"
        else
            password="$2"
        fi

        egrep "^$username" /etc/passwd >/dev/null
        if [ $? -eq 0 ]; then
            echo "Failed to add a user! $username has already exists!"
            exit 3
        else
            password_encrypted=$(perl -e 'print crypt($ARGV[0], "password")' $password)
            useradd -m -p $password_encrypted $username
            if [ $? -eq 0 ]; then
                echo "$username  ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
                echo "User $username has been created and added to sudoers list!"
            else
                echo "Failed to add a user!"
                exit 4
            fi
        fi
    fi
else
        echo "Only root may add a user to the system"
        exit 1
fi
