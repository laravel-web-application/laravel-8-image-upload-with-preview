<!DOCTYPE html>
<html>
<head>
    <title>Laravel 8 Image Upload with Preview Tutorial</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

</head>
<body>

<div class="container mt-4">

    @if(session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="card">

        <div class="card-header text-center font-weight-bold">
            <h2>Laravel 8 Preview Image Before Upload</h2>
        </div>

        <div class="card-body">
            <form method="POST" enctype="multipart/form-data" id="image-upload-preview" action="{{ url('upload') }}">
                @csrf
                <div class="row">

                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="file" name="image" placeholder="Choose image" id="image">
                            @error('name')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-12 mb-2">
                        <img id="preview-image" src="https://www.riobeauty.co.uk/images/product_image_not_found.gif"
                             alt="preview image" style="max-height: 250px;">
                    </div>

                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
    <script type="text/javascript">
        $('#image').change(function () {

            let reader = new FileReader();

            reader.onload = (e) => {

                $('#preview-image').attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);

        });
    </script>
</div>
</body>
</html>
