#!/bin/bash
# create a new user and add it to sudoers list
-- Create New User
username=$1
password=$2
mkdir -p /home/$username/.ssh
touch /home/$username/.ssh/authorized_keys
useradd -d /home/$username $password
usermod -aG sudo $username
chown -R $username:$username /home/$username/
chown root:root /home/$username
chmod 700 /home/$username/.ssh
chmod 644 /home/$username/.ssh/authorized_keys
